-- MySQL dump 10.13  Distrib 8.0.21, for Linux (x86_64)
--
-- Host: std-mysql    Database: std_1202_online_cinema
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (3,'Абхазия'),(4,'Австралия'),(5,'Австрия'),(6,'Азербайджан'),(7,'Албания'),(8,'Алжир'),(9,'Ангола'),(10,'Ангуилья'),(11,'Андорра'),(12,'Антигуа и Барбуда'),(13,'Антильские о-ва'),(14,'Аргентина'),(15,'Армения'),(16,'Арулько'),(17,'Афганистан'),(18,'Багамские о-ва'),(19,'Бангладеш'),(20,'Барбадос'),(21,'Бахрейн'),(22,'Беларусь'),(23,'Белиз'),(24,'Бельгия'),(25,'Бенин'),(26,'Бермуды'),(27,'Болгария'),(28,'Боливия'),(29,'Босния/Герцеговина'),(30,'Ботсвана'),(31,'Бразилия'),(32,'Британские Виргинские о-ва'),(33,'Бруней'),(34,'Буркина Фасо'),(35,'Бурунди'),(36,'Бутан'),(37,'Валлис и Футуна о-ва'),(38,'Вануату'),(39,'Великобритания'),(40,'Венгрия'),(41,'Венесуэла'),(42,'Восточный Тимор'),(43,'Вьетнам'),(44,'Габон'),(45,'Гаити'),(46,'Гайана'),(47,'Гамбия'),(48,'Гана'),(49,'Гваделупа'),(50,'Гватемала'),(51,'Гвинея'),(52,'Гвинея-Бисау'),(53,'Германия'),(54,'Гернси о-в'),(55,'Гибралтар'),(56,'Гондурас'),(57,'Гонконг'),(58,'Гренада'),(59,'Гренландия'),(60,'Греция'),(61,'Грузия'),(62,'Дания'),(63,'Джерси о-в'),(64,'Джибути'),(65,'Доминиканская республика'),(66,'Египет'),(67,'Замбия'),(68,'Западная Сахара'),(69,'Зимбабве'),(70,'Израиль'),(71,'Индия'),(72,'Индонезия'),(73,'Иордания'),(74,'Ирак'),(75,'Иран'),(76,'Ирландия'),(77,'Исландия'),(78,'Испания'),(79,'Италия'),(80,'Йемен'),(81,'Кабо-Верде'),(82,'Казахстан'),(83,'Камбоджа'),(84,'Камерун'),(85,'Канада'),(86,'Катар'),(87,'Кения'),(88,'Кипр'),(89,'Кирибати'),(90,'Китай'),(91,'Колумбия'),(92,'Коморские о-ва'),(93,'Конго (Brazzaville)'),(94,'Конго (Kinshasa)'),(95,'Коста-Рика'),(96,'Кот-д\'\'Ивуар'),(97,'Куба'),(98,'Кувейт'),(99,'Кука о-ва'),(100,'Кыргызстан'),(101,'Лаос'),(102,'Латвия'),(103,'Лесото'),(104,'Либерия'),(105,'Ливан'),(106,'Ливия'),(107,'Литва'),(108,'Лихтенштейн'),(109,'Люксембург'),(110,'Маврикий'),(111,'Мавритания'),(112,'Мадагаскар'),(113,'Македония'),(114,'Малави'),(115,'Малайзия'),(116,'Мали'),(117,'Мальдивские о-ва'),(118,'Мальта'),(125,'Марокко'),(119,'Мартиника о-в'),(120,'Мексика'),(121,'Мозамбик'),(122,'Молдова'),(123,'Монако'),(124,'Монголия'),(126,'Мьянма (Бирма)'),(127,'Мэн о-в'),(128,'Намибия'),(129,'Науру'),(130,'Непал'),(131,'Нигер'),(132,'Нигерия'),(133,'Нидерланды (Голландия)'),(134,'Никарагуа'),(135,'Новая Зеландия'),(136,'Новая Каледония о-в'),(137,'Норвегия'),(138,'Норфолк о-в'),(139,'О.А.Э.'),(140,'Оман'),(141,'Пакистан'),(142,'Панама'),(143,'Папуа Новая Гвинея'),(144,'Парагвай'),(145,'Перу'),(146,'Питкэрн о-в'),(147,'Польша'),(148,'Португалия'),(149,'Пуэрто Рико'),(150,'Реюньон'),(1,'Россия'),(151,'Руанда'),(152,'Румыния'),(154,'Сальвадор'),(155,'Самоа'),(156,'Сан-Марино'),(157,'Сан-Томе и Принсипи'),(158,'Саудовская Аравия'),(159,'Свазиленд'),(160,'Святая Люсия'),(161,'Святой Елены о-в'),(162,'Северная Корея'),(163,'Сейшеллы'),(164,'Сен-Пьер и Микелон'),(165,'Сенегал'),(166,'Сент Китс и Невис'),(167,'Сент-Винсент и Гренадины'),(168,'Сербия'),(169,'Сингапур'),(170,'Сирия'),(171,'Словакия'),(172,'Словения'),(173,'Соломоновы о-ва'),(174,'Сомали'),(175,'Судан'),(176,'Суринам'),(153,'США'),(177,'Сьерра-Леоне'),(178,'Таджикистан'),(180,'Таиланд'),(179,'Тайвань'),(181,'Танзания'),(182,'Того'),(183,'Токелау о-ва'),(184,'Тонга'),(185,'Тринидад и Тобаго'),(186,'Тувалу'),(187,'Тунис'),(188,'Туркменистан'),(189,'Туркс и Кейкос'),(190,'Турция'),(191,'Уганда'),(192,'Узбекистан'),(2,'Украина'),(193,'Уругвай'),(194,'Фарерские о-ва'),(195,'Фиджи'),(196,'Филиппины'),(197,'Финляндия'),(198,'Франция'),(199,'Французская Гвинея'),(200,'Французская Полинезия'),(201,'Хорватия'),(202,'Чад'),(203,'Черногория'),(204,'Чехия'),(205,'Чили'),(206,'Швейцария'),(207,'Швеция'),(208,'Шри-Ланка'),(209,'Эквадор'),(210,'Экваториальная Гвинея'),(211,'Эритрея'),(212,'Эстония'),(213,'Эфиопия'),(214,'ЮАР'),(215,'Южная Корея'),(216,'Южная Осетия'),(217,'Ямайка'),(218,'Япония');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` VALUES (1,'аниме'),(2,'боевик'),(3,'детектив'),(14,'документальный'),(4,'драма'),(5,'исторический'),(6,'комедия'),(7,'мелодрама'),(8,'мультфильм'),(9,'научный'),(16,'семейный'),(13,'спорт'),(12,'триллер'),(10,'ужасы'),(11,'фантастика'),(15,'фэнтези');
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `production_year` year(4) NOT NULL,
  `country_id` int(11) NOT NULL,
  `producer_id` int(11) NOT NULL,
  `scriptwriter_id` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `movie_country` (`country_id`),
  KEY `movie_producer` (`producer_id`),
  KEY `movie_scriptwriter` (`scriptwriter_id`),
  CONSTRAINT `movie_country` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `movie_producer` FOREIGN KEY (`producer_id`) REFERENCES `person` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `movie_scriptwriter` FOREIGN KEY (`scriptwriter_id`) REFERENCES `person` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie`
--

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
INSERT INTO `movie` VALUES (19,'Интерстеллар','Наше время на Земле подошло к концу, команда исследователей берет на себя самую важную миссию в истории человечества; путешествуя за пределами нашей галактики, чтобы узнать есть ли у человечества будущее среди звезд.\r\nКартинки по запросу интерстеллар описание',2015,153,5,6,169),(25,'Аватар','Джейк Салли - бывший морской пехотинец, прикованный к инвалидному креслу. Несмотря на немощное тело, Джейк в душе по-прежнему остается воином. Он получает задание совершить путешествие в несколько световых лет к базе землян на планете Пандора, где корпорации добывают редкий минерал, имеющий огромное значение для выхода Земли из энергетического кризиса.',2009,153,11,11,162),(26,'На твоей волне','Первокурсница Хинако переезжает в небольшой прибрежный городок, где провела детство, и знакомится с пожарным Минато, когда тот вывозит её с крыши во время пожара. Молодые люди разделяют увлечение сёрфингом и, проводя вместе всё больше времени, влюбляются друг в друга. Но как бы ни были сильны чувства девушки к Минато, парень считает, что должен посвятить жизнь спасению других, и в один ужасный день погибает. Хинако никак не может прийти в себя после потери, но вскоре Минато начинает являться ей в различных жидкостях.',2019,218,14,15,95),(28,'1+1','Пострадав в результате несчастного случая, богатый аристократ Филипп нанимает в помощники человека, который менее всего подходит для этой работы, – молодого жителя предместья Дрисса, только что освободившегося из тюрьмы. Несмотря на то, что Филипп прикован к инвалидному креслу, Дриссу удается привнести в размеренную жизнь аристократа дух приключений.',2011,198,19,19,112),(29,'Соседи по комнате','Влюбленная парочка Аои Нисимори и Сюсэй Кугаяма живёт вместе, но они скрывают этот факт ото всех. Однажды об этом узнает двоюродный брат Сюсэя, Рэон, и начинает жить вместе с ними.',2019,218,23,22,107),(31,'В первый раз','Симпатяга Дэйв безнадежно и бесперспективно влюблен в первую школьную красавицу. Очаровательная Обри встречается с самодовольным мачо, который ей совершенно не подходит, но с которым она, зачем-то, решила лишиться девственности. И совсем неизвестно, как бы обернулись жизни двух этих замечательных молодых людей, если бы они случайно не столкнулись на вечеринке, которую разогнала полиция.\n\nОбри и Дэйв становятся друзьями… нет, вы не подумайте — только друзьями, которые могут поплакаться друг другу в жилетку и пожаловаться на превратности любви. Но однажды их нежная дружба начинает под воздействием сильного эротического электричества превращаться во что-то большее — захватывающее и волнующее. В то, что у каждого когда-то бывает в первый раз… и о чем потом, бывает, смешно вспоминать.',2012,153,26,26,95),(33,'Оно','Когда в городке Дерри штата Мэн начинают пропадать дети, несколько ребят сталкиваются со своими величайшими страхами - не только с группой школьных хулиганов, но со злобным клоуном Пеннивайзом, чьи проявления жестокости и список жертв уходят в глубь веков.',2017,153,29,30,135),(34,'Безумная свадьба','Главные герои фильма, мсье Верней и его жена – добропорядочная буржуазная французская супружеская пара, родители четырех красавиц-дочерей. Как известно, основная проблема всех подобных семей – выдать дочек замуж, чтобы не засиделись в девках. У семейства Верней такой проблемы нет. Три дочери уже нашли свое счастье, вот только мужья, по мнению родителей, подкачали: один – китаец, другой – араб, третий – еврей… Пришло время, и четвертая дочь сообщает радостную весть: она тоже выходит замуж! И ее жених… чернокожий.',2014,198,33,33,94),(38,'Золушка','Отец молодой девушки по имени Элла, овдовев, женится во второй раз, и вскоре Элла оказывается один на один с жадными и завистливыми новыми родственницами – мачехой Леди Тремэйн и ее дочерьми - Анастасией и Дризеллой. Из хозяйки дома она превращается в служанку, вечно испачканную золой, за что и получает от своих сварливых сводных сестриц прозвище – Золушка.\n\nНесмотря на злоключения, выпавшие на ее долю, Золушка не отчаивается, ведь даже в самые тяжелые моменты находится что-то, что помогает ей думать о хорошем: например, случайная встреча на лесной тропинке с прекрасным юношей. Элла даже не предполагает, что встретилась с самим принцем и что вскоре Фея-крестная навсегда поменяет её жизнь к лучшему.',2015,39,38,39,105),(41,'Аэроплан','Бывший военный лётчик оказывается на борту самолёта, экипаж которого из-за пищевого отравления не в состоянии управлять аэробусом. Вместе со стюардессой герой вынужден стать спасителями пестрой компании пассажиров, оказавшихся на грани гибели. Новоявленного пилота и его помощницу ждут немыслимые трудности и невероятные приключения.',1980,153,43,44,88);
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_actor`
--

DROP TABLE IF EXISTS `movie_actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movie_actor` (
  `movie_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  PRIMARY KEY (`movie_id`,`person_id`) USING BTREE,
  KEY `actor_person` (`person_id`),
  CONSTRAINT `actor_movie` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `actor_person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_actor`
--

LOCK TABLES `movie_actor` WRITE;
/*!40000 ALTER TABLE `movie_actor` DISABLE KEYS */;
INSERT INTO `movie_actor` VALUES (19,7),(19,8),(19,9),(19,10),(25,12),(25,13),(26,16),(26,17),(26,18),(28,20),(28,21),(29,24),(29,25),(31,27),(31,28),(33,31),(33,32),(34,34),(34,35),(34,36),(34,37),(38,40),(38,41),(38,42),(41,45),(41,46),(41,47);
/*!40000 ALTER TABLE `movie_actor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_genre`
--

DROP TABLE IF EXISTS `movie_genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movie_genre` (
  `movie_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  PRIMARY KEY (`genre_id`,`movie_id`) USING BTREE,
  KEY `movie_movie` (`movie_id`),
  CONSTRAINT `genre_genre` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `movie_movie` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_genre`
--

LOCK TABLES `movie_genre` WRITE;
/*!40000 ALTER TABLE `movie_genre` DISABLE KEYS */;
INSERT INTO `movie_genre` VALUES (19,4),(19,9),(19,11),(25,2),(25,4),(25,11),(26,1),(26,4),(26,7),(26,8),(26,15),(28,4),(28,6),(29,6),(29,7),(31,6),(31,7),(33,3),(33,4),(33,10),(33,15),(34,6),(38,4),(38,7),(38,15),(38,16),(41,6);
/*!40000 ALTER TABLE `movie_genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (5,'Кристофер Нолан'),(6,'Эмма Томас'),(7,'Мэттью Макконахи'),(8,'Энн Хэтэуэй'),(9,'Джессика Честейн'),(10,'Майкл Кейн'),(11,'Джеймс Кэмерон'),(12,'Сэм Уортингтон'),(13,'Зои Салдана'),(14,'Масааки Юаса'),(15,'Рэйко Ёсида'),(16,'Рина Каваэи'),(17,'Рёта Катаёсэ'),(18,'Хонока Мацумото'),(19,'Оливье Накаш'),(20,'Франсуа Клюзе'),(21,'Омар Си'),(22,'Митиру Эгасира'),(23,'Ясухиро Кавамура'),(24,'Монэ Камисираиси'),(25,'Ёсукэ Сугино'),(26,'Джон Кэздан'),(27,'Бритт Робертсон'),(28,'Дилан О’Брайен'),(29,'Андрес Мускетти'),(30,'Чейз Палмер'),(31,'Джейден Мартелл'),(32,'Джереми Рэй Тейлор'),(33,'Филипп де Шоврон'),(34,'Кристиан Клавье'),(35,'Шанталь Лоби'),(36,'Эри Абиттан'),(37,'Меди Садун'),(38,'Кеннет Брана'),(39,'Крис Вайц'),(40,'Лили Джеймс'),(41,'Кейт Бланшетт'),(42,'Ричард Мэдден'),(43,'Джим Абрахамс'),(44,'Артур Хейли'),(45,'Роберт Хэйс'),(46,'Джули Хэгерти'),(47,'Питер Грейвз');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poster`
--

DROP TABLE IF EXISTS `poster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `poster` (
  `id` varchar(36) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `mime_type` varchar(64) NOT NULL,
  `md5_hash` varchar(100) NOT NULL,
  `movie_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `md5_hash` (`md5_hash`),
  UNIQUE KEY `movie_id` (`movie_id`),
  CONSTRAINT `poster_movie` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poster`
--

LOCK TABLES `poster` WRITE;
/*!40000 ALTER TABLE `poster` DISABLE KEYS */;
INSERT INTO `poster` VALUES ('078e338a-78c3-4a39-8990-605e2115b340','600x900_4.webp','image/webp','947382debdf7bf1455e69f52249210c0',31),('37cf5626-d3cb-4855-ae1e-f2690191c1a9','600x900_7.webp','image/webp','cf481d4fd76087fd7adeeb56eefdfe6d',38),('401221f6-1289-433b-8f05-b7d61ab0b309','600x900_5.webp','image/webp','164e462cfd65695b3761df8a392cdd1e',33),('4821bd5c-4b7e-4b0b-879e-63d0aa437f4e','600x900.webp','image/webp','da9c8709abc61204f0a909f955a2c8ae',25),('4c2607c0-3c47-4898-841a-5681b8c8eea2','600x900_1.webp','image/webp','9ca2b204fae8048135b0a26b9fae7398',26),('724c99ed-c5f7-4578-ab63-050f48eb2acf','600x900_8.webp','image/webp','d980390b4e48d9336ee32d3624fabb42',41),('8eaed045-be03-4d69-bb8e-02d43c729cf9','600x900_2.webp','image/webp','f6e495523bb931893842e3a83ca63400',28),('b00fba46-c0a7-4304-9060-f765febd680b','600x900.webp','image/webp','88e68e980c0464be54d64ca43cc6a694',19),('b9f82bbe-2ead-4d92-a46f-e61137d9d6b9','600x900_3.webp','image/webp','fab96124b75289e26dda1b1092fd4770',29),('c0a2d04b-f937-4811-998e-4c7044dc33e9','600x900_6.webp','image/webp','170447dd91f0e4233992c2872abd3908',34);
/*!40000 ALTER TABLE `poster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `raiting` int(11) NOT NULL,
  `text` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `review_movie` (`movie_id`),
  KEY `review_user` (`user_id`),
  CONSTRAINT `review_movie` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `review_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'администратор','суперпользователь, имеет полный доступ к системе, в том числе к созданию и удалению фильмов'),(2,'модератор','может редактировать фильмы и производить модерацию рецензий'),(3,'пользователь','может оставлять рецензии');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(64) NOT NULL,
  `password_hash` varchar(100) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `middle_name` varchar(64) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  KEY `user_role` (`role_id`),
  CONSTRAINT `user_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918','Михайлов','Артем','',1),(2,'manager','6ee4a469cd4e91053847f5d3fcb61dbcc91e8f0ef10be7748da4c4a1ba382d17','Кружалов','Алексей','Сергеевич',2),(3,'user','04f8996da763b7a969b1028ee3007569eaf3a635486ddab211d512c85b9df8fb','Гневшев','Александр','Юрьевич',3);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-26 16:45:07
