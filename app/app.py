from flask import Flask, session, render_template, url_for, request, make_response, redirect, flash, send_from_directory
from flask_login import login_required, current_user
from mysql_db import MySQL
import mysql.connector as connector
import markdown
import os
from uuid import uuid4
import bleach

PER_PAGE = 8

app = Flask(__name__)
application = app

app.config.from_pyfile('config.py')

mysql = MySQL(app)

from auth import bp as auth_bp, init_login_manager, check_rights
from review import bp as review_bp
from tools import *
init_login_manager(app)
# new blueprint movie (from this file)
app.register_blueprint(auth_bp)
app.register_blueprint(review_bp)


@app.route('/')
def index():
    page = request.args.get('page', 1, type=int)
    query='SELECT count(*) AS count FROM movie;'
    pagination_info = paginate(page,PER_PAGE,query)
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('''SELECT movie.*, GROUP_CONCAT(genre.name ORDER BY genre.name SEPARATOR ', ') as genres, ( SELECT COUNT(*) FROM review where review.status_id = 2 AND review.movie_id = movie.id ) as reviews FROM `movie` JOIN movie_genre on movie.id=movie_genre.movie_id JOIN genre on movie_genre.genre_id=genre.id GROUP BY movie.id ORDER BY movie.production_year DESC LIMIT %s OFFSET %s;''', (PER_PAGE, PER_PAGE*(page -1)))
    movies = cursor.fetchall()
    cursor.close()
    return render_template('index.html', movies=movies, pagination_info=pagination_info)


@app.route('/edit/<int:movie_id>')
@login_required
@check_rights('edit')
def edit(movie_id):
    choosed = from_id_choosed(movie_id)
    return render_template('edit.html',genres=load_genres(), countries=load_countries(), persons=load_persons(), choosed=choosed, movie_id = movie_id)

@app.route('/update/<int:movie_id>', methods=['POST'])
@login_required
@check_rights('edit')
def update(movie_id):
    choosed = form_choosed()
    cursor = mysql.connection.cursor(named_tuple=True)
    try:
        cursor.execute('''UPDATE movie SET
        title = %s, description = %s, production_year = %s, country_id = %s, producer_id = %s, scriptwriter_id = %s, duration = %s WHERE id = %s  ''',
        (choosed['title'],choosed['description'],choosed['year'],choosed['country'],choosed['producer'],choosed['scriptwriter'],choosed['duration'],movie_id))
        cursor.execute('DELETE FROM movie_actor WHERE movie_id = %s', (movie_id,))
        for actor in choosed['actors']:
            cursor.execute(
            'INSERT INTO movie_actor (movie_id, person_id) VALUES (%s, %s)', (movie_id,actor))
        cursor.execute('DELETE FROM movie_genre WHERE movie_id = %s', (movie_id,))
        for genre in choosed['genres']:
            cursor.execute(
            'INSERT INTO movie_genre (movie_id, genre_id) VALUES (%s, %s) ', (movie_id,genre))
        cursor.close()
    except connector.errors.DatabaseError:
        mysql.connection.rollback()
        flash('Введены некоректные данные. Ошибка сохранения', 'danger')
        return render_template('edit.html', genres=load_genres(), countries=load_countries(), persons=load_persons(), choosed=choosed, movie_id=movie_id)
    mysql.connection.commit()
    cursor.close()
    flash(f'Фильм был успешно обновлён', 'success')
    return redirect(url_for('index'))

@app.route('/show/<int:movie_id>')
def show(movie_id):
    mrkdwn = {}
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('SELECT * FROM movie where id = %s;', (movie_id,))
    movie = cursor.fetchone()
    mrkdwn['movie'] = markdown.markdown(movie.description)
    cursor.execute('SELECT g.name FROM movie_genre mg JOIN genre g on g.id = mg.genre_id WHERE mg.movie_id = %s;', (movie.id,))
    genres =  [str(item.name) for item in cursor.fetchall()]
    genres = ', '.join(genres).rstrip(',')
    cursor.execute('SELECT p.name FROM movie_actor ma JOIN person p on p.id = ma.person_id WHERE ma.movie_id = %s;', (movie.id,))
    actors =  [str(item.name) for item in cursor.fetchall()]
    actors = ', '.join(actors).rstrip(',')
    cursor.execute('SELECT name FROM country where id = %s;', (movie.country_id,))
    country = cursor.fetchone()
    cursor.execute('SELECT name FROM person where id = %s;', (movie.producer_id,))
    producer = cursor.fetchone()
    cursor.execute('SELECT name FROM person where id = %s;', (movie.scriptwriter_id,))
    scriptwriter = cursor.fetchone()
    if current_user.is_authenticated:
        cursor.execute('SELECT r.text, r.rating, r.date, u.surname, u.name, u.middle_name,r.status_id FROM review r JOIN user u on r.user_id = u.id WHERE r.movie_id = %s and r.user_id != %s and r.status_id = %s;', (movie.id,current_user.id,2))
        reviews = cursor.fetchall()
        cursor.execute('SELECT r.text, r.rating, r.date, u.surname, u.name as name , u.middle_name,r.status_id, rs.name as status_name FROM review r JOIN user u on r.user_id = u.id JOIN review_status rs on rs.id = r.status_id WHERE r.movie_id = %s and r.user_id = %s;', (movie.id,current_user.id))
        your_review = cursor.fetchone()
        mrkdwn['reviews'] = [ markdown.markdown(review.text) for review in reviews]
        if your_review is not None:
            mrkdwn['your_review'] =  markdown.markdown(your_review.text)
    else:
        cursor.execute('SELECT r.text, r.rating, r.date, u.surname, u.name, u.middle_name,r.status_id FROM review r JOIN user u on r.user_id = u.id WHERE r.movie_id = %s and r.status_id = %s;', (movie.id,2))
        reviews = cursor.fetchall()
        mrkdwn['reviews'] = [ markdown.markdown(review.text) for review in reviews]
        your_review = None
    cursor.close()
    
    return render_template('show.html', movie=movie, genres=genres,country = country.name, producer=producer.name,scriptwriter=scriptwriter.name, actors=actors, reviews=reviews, mrkdwn=mrkdwn,your_review=your_review)


@app.route('/new')
@login_required
@check_rights('new')
def new():
    return render_template('new.html', genres=load_genres(), countries=load_countries(), persons=load_persons(), choosed={})

@app.route('/create', methods=['GET', 'POST'])
@login_required
@check_rights('new')
def create():
    id= uuid4()
    choosed = form_choosed()
    try:
        cursor = mysql.connection.cursor(named_tuple=True)
        cursor.execute(
            '''INSERT INTO movie (title, description, production_year, country_id, producer_id, scriptwriter_id, duration)
            VALUES ( %s, %s, %s, %s, %s, %s, %s);''', (choosed['title'],choosed['description'],choosed['year'],choosed['country'],choosed['producer'],choosed['scriptwriter'],choosed['duration']))
        movie_id = cursor.lastrowid
        for actor in choosed['actors']:
            cursor.execute(
            'INSERT INTO movie_actor (movie_id, person_id) VALUES (%s, %s)', (movie_id,actor))   
        for genre in choosed['genres']:
            cursor.execute(
            'INSERT INTO movie_genre (movie_id, genre_id) VALUES (%s, %s) ', (movie_id,genre))
        cursor.close()
    except connector.errors.DatabaseError:
        mysql.connection.rollback()
        flash('Введены некоректные данные. Ошибка сохранения', 'danger')
        return render_template('new.html', genres=load_genres(), countries=load_countries(), persons=load_persons(), choosed=choosed)
    #получает фотографию с формы
    f = request.files.get('background_img')
    if not f:
        flash('Не выбрано фото для постера', 'danger')
        return render_template('new.html', genres=load_genres(), countries=load_countries(), persons=load_persons(), choosed=choosed)
    #Создается объекс класа PosterSaver из полученного фото
    poster_saver = PosterSaver(f, movie_id)
    poster_img_exist = poster_saver.save()
    if poster_img_exist == 'DB error':
        flash('Произошла ошибка с БД, попробуйте ещё раз', 'danger')
        return render_template('new.html', genres=load_genres(), countries=load_countries(), persons=load_persons(), choosed=choosed)
    elif poster_img_exist:
        flash('Данное фото уже используется для постера, возмжно данный фильм уже существует', 'danger')
        return render_template('new.html', genres=load_genres(), countries=load_countries(), persons=load_persons(), choosed=choosed)
    flash(f'Фильм добавлен', 'success')
    return redirect(url_for('index'))

@app.route('/delete/<int:movie_id>', methods=['POST'])
@login_required
@check_rights('delete')
def delete(movie_id):
    with mysql.connection.cursor(named_tuple=True) as cursor:
        try:
            cursor.execute('SELECT * FROM poster where movie_id = %s',(movie_id,))
            poster = cursor.fetchone()
            cursor.execute('DELETE FROM movie WHERE id = %s;', (movie_id,))
        except connector.errors.DatabaseError:
            flash('Не удалось удалить запись.', 'danger')
            return redirect(url_for('index'))
        mysql.connection.commit()
        _, ext = os.path.splitext(poster.file_name)
        os.remove(os.path.join(current_app.config['UPLOAD_FOLDER'],poster.id + ext))
        flash('Запись была успешна удалена!', 'success')
    return redirect(url_for('index'))

@app.route('/poster/<movie_id>')
def poster(movie_id):
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('SELECT * FROM poster where movie_id = %s;', (movie_id,))
    poster = cursor.fetchone()
    cursor.close()
    if poster is None:
        abort(404)
    _, ext = os.path.splitext(poster.file_name)
    return send_from_directory(app.config['UPLOAD_FOLDER'], poster.id + ext)
    


@app.route('/new/person')
@login_required
@check_rights('new')
def person():
    return render_template('person.html')

@app.route('/create/person', methods=['POST'])
@login_required
@check_rights('new')
def create_person():
    person = request.form.get('person')
    try:
        cursor = mysql.connection.cursor(named_tuple=True)
        cursor.execute('INSERT INTO person (name) VALUES (%s)', (person,))
        cursor.close()
    except connector.errors.DatabaseError:
            flash('Не удалось добавить пользователя.', 'danger')
            return redirect(url_for('index'))
    flash('Пользователь успешно добавлен.', 'success')
    mysql.connection.commit()
    return redirect(url_for('index'))

