$('#delete-movie-modal').on('show.bs.modal', function (event) {
    let url = event.relatedTarget.dataset.url;
    let form = this.querySelector('form');
    form.action = url;
    let userName = event.relatedTarget.closest('div').querySelector('.card-title').textContent;
    this.querySelector('#movie-title').textContent = "\"" + userName + "\"";
})