from flask import  request, make_response, current_app #  current_app нужен для того чтобы перенести ImageSaver так как в этой переменной будет хранится наше приложение app а мы не хотим его пересылать каждый раз
import math
from app import mysql
import os
import hashlib
from werkzeug.utils import secure_filename
from uuid import uuid4
import bleach

class PosterSaver:
    def __init__(self, file, movie_id):
        self.file = file
        self.movie_id = movie_id

    def save(self):
        self.poster = self.__find_by_md5_hash()
        if self.poster is not None:
            return True
        self.file_name = secure_filename(self.file.filename)
        self.id = str(uuid4())
        try:
            cursor = mysql.connection.cursor(named_tuple=True)
            cursor.execute('INSERT INTO poster (id, file_name, mime_type, md5_hash,movie_id) VALUES (%s, %s, %s, %s,%s);', (self.id,self.file_name,self.file.mimetype, self.md5_hash, self.movie_id))
            cursor.close()        
        except connector.errors.DatabaseError:
            return 'DB error'
        self.file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], self.storage_filename))
        mysql.connection.commit()
        return False

    @property
    def storage_filename(self):
        _, ext = os.path.splitext(self.file_name)
        return self.id + ext

    def __find_by_md5_hash(self):
        self.md5_hash = hashlib.md5(self.file.read()).hexdigest()
        self.file.seek(0)
        cursor = mysql.connection.cursor(named_tuple=True)
        cursor.execute('Select * from poster where md5_hash = %s;', (self.md5_hash,))
        file_here = cursor.fetchone() 
        cursor.close()
        return file_here


def paginate(page,per_page,query):
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute(query)
    total_count = cursor.fetchone().count
    cursor.close()
    total_pages = math.ceil(total_count/per_page)
    return  {
        'current_page': page,
        'total_pages': total_pages,
        'per_page': per_page
    }
    
def from_id_choosed(movie_id):
    choosed = {}
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('SELECT * FROM movie where id = %s;', (movie_id,))
    movie = cursor.fetchone()
    choosed['title'] = movie.title
    choosed['year'] = movie.production_year
    choosed['duration'] = movie.duration
    choosed['description'] = movie.description
    cursor.execute('SELECT g.name, g.id FROM movie_genre mg JOIN genre g on g.id = mg.genre_id WHERE mg.movie_id = %s;', (movie.id,))
    choosed['genres'] = [int(item.id) for item in cursor.fetchall()]
    cursor.execute('SELECT p.name, p.id FROM movie_actor ma JOIN person p on p.id = ma.person_id WHERE ma.movie_id = %s;', (movie.id,))
    choosed['actors'] = [int(item.id) for item in cursor.fetchall()]
    cursor.execute('SELECT * FROM country where id = %s;', (movie.country_id,))
    choosed['country'] = cursor.fetchone().id
    cursor.execute('SELECT * FROM person where id = %s;', (movie.producer_id,))
    choosed['producer'] = cursor.fetchone().id
    cursor.execute('SELECT * FROM person where id = %s;', (movie.scriptwriter_id,))
    choosed['scriptwriter'] = cursor.fetchone().id
    cursor.close()
    return choosed

def form_choosed():
    return {
        'title': request.form.get('title'),
        'year': request.form.get('year'),
        'duration': request.form.get('duration'),
        'country': int(request.form.get('country')),
        'genres': [int(item) for item in request.form.getlist('genres')],
        'producer': int(request.form.get('producer')),
        'scriptwriter': int(request.form.get('scriptwriter')),
        'actors': [int(item) for item in request.form.getlist('actors')],
        'description': bleach.clean(request.form.get('description')),
    }

def load_genres():
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('SELECT * FROM genre;')
    genres = cursor.fetchall()
    cursor.close()
    return genres

def load_countries():
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('SELECT * FROM country;')
    countries = cursor.fetchall()
    cursor.close()
    return countries

def load_persons():
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('SELECT * FROM person;')
    persons = cursor.fetchall()
    cursor.close()
    return persons
