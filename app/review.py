from flask import Blueprint, session, render_template, url_for, request, make_response, redirect, flash, send_from_directory
from flask_login import login_required, current_user
from app import mysql
from auth import  check_rights
from tools import *
import markdown
import mysql.connector as connector
PER_PAGE = 4

bp = Blueprint('review', __name__, url_prefix='/review')

@bp.route('/new/review/<int:movie_id>')
@login_required
def review_add(movie_id):
    cursor = mysql.connection.cursor(named_tuple=True)
    #если кто-то хочет но у него не получится если нельзя
    try:
        cursor.execute('SELECT * FROM review WHERE user_id = %s and movie_id = %s;',(current_user.id,movie_id))
        review_here = cursor.fetchone()
        cursor.close()
        if review_here is not None:
            flash('Вы уже оставили отзыв!', 'danger')
            return redirect(url_for('show', movie_id=movie_id))
        cursor = mysql.connection.cursor(named_tuple=True)
        cursor.execute('SELECT * FROM movie WHERE id = %s;',(movie_id,))
        movie_here = cursor.fetchone()
        cursor.close()
        if movie_here is None:
            flash('Данного фильма не существует!', 'danger')
            return redirect(url_for('index'))
    except connector.errors.DatabaseError:
        return redirect(url_for('review.review_add', movie_id=movie_id))
    return render_template('reviews/review.html',movie_id=movie_id)


@bp.route('/moderation/<int:review_id>')
@login_required
@check_rights('edit')
def moderation(review_id):
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('SELECT m.title,r.text, r.rating, r.date, u.surname, u.name as name , u.middle_name,r.id FROM review r JOIN user u on r.user_id = u.id JOIN movie m on m.id = r.movie_id WHERE r.id = %s;', (review_id,))
    review = cursor.fetchone()
    mrkdwn = markdown.markdown(review.text)
    return render_template('reviews/moderation.html', review=review,mrkdwn=mrkdwn)

@bp.route('/moderate')
@login_required
@check_rights('edit')
def moderate():
    review_id = request.args.get('review_id')
    do = request.args.get('do')
    try:
        if do == '2':
            cursor = mysql.connection.cursor(named_tuple=True)
            cursor.execute('UPDATE review SET status_id = 2 WHERE id = %s ', (review_id,))
        else:
            cursor = mysql.connection.cursor(named_tuple=True)
            cursor.execute('UPDATE review SET status_id = 3 WHERE id = %s ', (review_id,))            
    except connector.errors.DatabaseError:
        flash('Произошла ошибка,попробуйте снова!', 'danger')
        return redirect(url_for('review.moderation', review_id=review_id))
    mysql.connection.commit()
    return redirect(url_for('review.review_moderation'))


@bp.route('/my_reviews/<int:user_id>')
@login_required
def my_reviews(user_id):
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('SELECT m.title,r.text, r.rating, r.date, u.surname, u.name as name , u.middle_name,r.status_id, rs.name as status_name FROM review r JOIN user u on r.user_id = u.id JOIN review_status rs on rs.id = r.status_id JOIN movie m on m.id = r.movie_id WHERE r.user_id = %s;', (user_id,))
    reviews = cursor.fetchall()
    mrkdwn = [ markdown.markdown(review.text) for review in reviews]
    return render_template('reviews/my_reviews.html', reviews=reviews,mrkdwn=mrkdwn)

@bp.route('/review_moderation')
@login_required
@check_rights('edit')
def review_moderation():
    page = request.args.get('page', 1, type=int)
    query='SELECT count(*) as count FROM review r JOIN user u on r.user_id = u.id JOIN movie m on r.movie_id = m.id WHERE r.status_id = 1;'
    pagination_info = paginate(page,PER_PAGE,query)
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('SELECT  m.title, r.date, u.surname, u.name, u.middle_name, r.id FROM review r JOIN user u on r.user_id = u.id JOIN movie m on r.movie_id = m.id  WHERE r.status_id = 1 ORDER BY r.date LIMIT %s OFFSET %s',(PER_PAGE, PER_PAGE*(page -1)))
    reviews = cursor.fetchall()
    return render_template('reviews/review_moderation.html', reviews=reviews, pagination_info=pagination_info)


@bp.route('/review/<int:movie_id>', methods=['POST'])
@login_required
def review(movie_id):
    rating = int(request.form.get('rating'))
    text = bleach.clean(request.form.get('text'))
    try:
        cursor = mysql.connection.cursor(named_tuple=True)
        cursor.execute('INSERT INTO review (movie_id, user_id, rating, text) VALUES (%s, %s, %s, %s);',(movie_id,current_user.id,rating,text))
        cursor.close()
    except connector.errors.DatabaseError:
        flash('Введены некоректные данные. Ошибка сохранения', 'danger')
        return render_template('reviews/review.html',rating=rating,text=text, movie_id=movie_id)
    mysql.connection.commit()
    flash('Комментарий был успешно создан!', 'success')
    return redirect(url_for('show', movie_id = movie_id))
