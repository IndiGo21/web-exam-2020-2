from functools import wraps
from flask import Blueprint, render_template, url_for, request, make_response, redirect, flash
from flask_login import LoginManager, UserMixin, login_user,  logout_user, login_required, current_user
from app import mysql
from users_policy import UsersPolicy

bp = Blueprint('auth', __name__, url_prefix='/auth')


class User(UserMixin):
    def __init__(self, user_id, login, role_id,surname,name,middle_name = None):
        super().__init__()
        self.id = user_id
        self.login = login
        self.role_id = role_id
        self.surname = surname
        self.name = name
        self.middle_name = middle_name

    @property
    def full_name(self):
        return ' '.join([self.surname, self.name, self.middle_name or ''])

    def can(self, action):
        policy = UsersPolicy()
        method = getattr(policy, action, None)
        if method:
            return method()
        return False

def check_rights(action):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if not current_user.can(action):
                flash('У вас недостаточно прав для доступа к данной странице', 'danger')
                return redirect(url_for('index'))
            return func(*args, **kwargs)
        return wrapper
    return decorator

def load_user(user_id):
    cursor = mysql.connection.cursor(named_tuple=True)
    cursor.execute('SELECT * FROM user Where id = %s;', (user_id,))
    user = cursor.fetchone()
    cursor.close()
    if user:
        return User(user_id=user.id, login=user.login, role_id=user.role_id, surname= user.surname, name= user.name, middle_name=user.middle_name )
    return None


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        login = request.form.get('login')
        password = request.form.get('password')
        remember_me = request.form.get('remember_me') == 'on'
        if login and password:
            cursor = mysql.connection.cursor(named_tuple=True)
            cursor.execute(
                'SELECT * FROM user Where login = %s AND password_hash = SHA2(%s, 256);', (login, password))
            user = cursor.fetchone()
            cursor.close()
            if user:
                user = User(user_id=user.id, login=user.login, role_id=user.role_id, surname= user.surname, name= user.name, middle_name=user.middle_name )
                login_user(user, remember=remember_me)

                flash('Вы успешно аутентифицированы.', 'success')

                next = request.args.get('next')

                return redirect(next or url_for('index'))
        flash('Невозможно аутентифицироваться с указанными логином и паролем', 'danger')
    return render_template('login.html')


@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


def init_login_manager(app):
    login_manager = LoginManager()
    login_manager.init_app(app)
    login_manager.login_view = 'auth.login'
    login_manager.login_message = 'Для доступа к данной странице необходимо пройти аутентификацию'
    login_manager.login_message_category = 'warning'
    login_manager.user_loader(load_user)
